@extends('layout.master')

@section('content')
	<section class="content">
	    <section class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <!-- <h1>Blank Page</h1> -->
	          </div>
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item"><a href="#">Home</a></li>
	              <li class="breadcrumb-item active">Blank Page</li>
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>	
		<div class="row">
	         <!-- left column -->
		    <div class="col-md-12">
		        <!-- general form elements -->
		    	<div class="card card-primary">
		          <div class="card-header">
		            <h3 class="card-title">New Item</h3>
		          </div>
		          <!-- /.card-header -->
		          <!-- form start -->
		          <form role="form" action="/jawaban" method="post">
		            @csrf
		            <div class="card-body">
		              <div class="form-group">
		                <label for="Isi">Isi</label>
		                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
		              </div>
		              <div class="form-group">
		                <label for="description">Tanggal Di Buat</label>
		                <input type="text" class="form-control" id="description" name="description" placeholder="description">
		              </div>
		              <div class="form-group">
		                <label for="price">Tanggal Di Perbarui</label>
		                <input type="number" class="form-control" id="price" name="price" placeholder="price">
		              </div>
		              <div class="form-group">
		                <label for="stock">Status Like</label>
		                <input type="number" class="form-control" id="stock" name="stock" placeholder="stock">
		              </div>

		            </div>
		            <!-- /.card-body -->

		            <div class="card-footer">
		              <button type="submit" class="btn btn-primary">Submit</button>
		            </div>
		          </form>
		        </div>
		        <!-- /.card -->
		    </div>
		</div>
	</section>

@endsection