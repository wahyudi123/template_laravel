@extends('layout.master')

@section('content')
	<section class="content">
	    <section class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <!-- <h1>Blank Page</h1> -->
	          </div>
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item"><a href="#">Home</a></li>
	              <li class="breadcrumb-item active">Blank Page</li>
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>	
		<div class="row">
	         <!-- left column -->
		    <div class="col-md-12">
		        <!-- general form elements -->
		    	<div class="card card-primary">
		          <div class="card-header">
		            <h3 class="card-title">New Topik</h3>
		          </div>
		          <!-- /.card-header -->
		          <!-- form start -->
		          <form role="form" action="/topik" method="post">
		            @csrf
		            <div class="card-body">
		              <div class="form-group">
		                <label for="nama">Nama Topik</label>
		                <input type="text" class="form-control" id="nama" name="nm_topik" placeholder="Masukkan Nama Topik">
		              </div>
		              <div class="form-group">
		                <label for="isi">Isi</label>
		                <input type="text" class="form-control" id="isi" name="isi_topik" placeholder="Masukkan Isi Topik">
		              </div>
		              <div class="form-group">
		                <label for="price">Waktu</label>
		                <input type="time" class="form-control" id="waktu" name="waktu" placeholder="price">
		              </div>
		              <div class="form-group">
		                <label for="poto">Foto</label>
		                <input type="text" class="form-control" id="poto" name="foto" placeholder="poto">
		              </div>

		            </div>
		            <!-- /.card-body -->

		            <div class="card-footer">
		              <button type="submit" class="btn btn-primary">Submit</button>
		            </div>
		          </form>
		        </div>
		        <!-- /.card -->
		    </div>
		</div>
	</section>

@endsection