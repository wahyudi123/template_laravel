<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/items/data');
});

Route::get('/data_tables', function () {
    return view('/items/d_table');
});

Route::get('/master', function () {
    return view('/layout/master');
});

Route::get('/items', function () {
    return view('/items/itemm');
});

//item action
Route::get('/items/create', 'ItemController@create'); //menampilkan hal;amn
Route::post('/items', 'ItemController@store'); //simpan data
Route::get('/items', 'ItemController@index'); //menampilan data
//item action

//topik action
Route::get('/topik/create', 'TopikController@create'); //menampilkan hal;amn
Route::post('/topik', 'TopikController@store'); //simpan data
Route::get('/topik', 'TopikController@index'); //menampilan data
//item action

//jawaban action
Route::get('/jawaban/create', 'JawabanController@create'); //menampilkan hal;amn
Route::post('/jawaban', 'JawabanController@store'); //simpan data
Route::get('/jawaban', 'JawabanController@index'); //menampilan data
//jawaban action