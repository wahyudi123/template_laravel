<?php 
	namespace App\Models;
	use Illuminate\Support\Facades\DB;
	
	class TopikModel {
		public static function get_all(){
			$items = DB::table('topik')->get();
			return $items;
		}

		public static function save($data){
			unset($data["_token"]);
			$new_items = DB::table('topik')->insert($data);
			return $new_items;
		}
	}
?>