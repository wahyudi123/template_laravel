<?php 
	namespace App\Models;
	use Illuminate\Support\Facades\DB;
	
	class ItemModel {
		public static function get_all(){
			$items = DB::table('item')->get();
			return $items;
		}

		public static function save($data){
			unset($data["_token"]);
			$new_items = DB::table('item')->insert($data);
			return $new_items;
		}
	}
?>