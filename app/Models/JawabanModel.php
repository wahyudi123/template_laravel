<?php 
	namespace App\Models;
	use Illuminate\Support\Facades\DB;
	
	class JawabanModel {
		public static function get_all(){
			$items = DB::table('jawaban')->get();
			return $items;
		}

		public static function save($data){
			unset($data["_token"]);
			$new_items = DB::table('jawaban')->insert($data);
			return $new_items;
		}
	}
?>