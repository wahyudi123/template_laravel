<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TopikModel;

class TopikController extends Controller
{
    public function index(){
    	$topiks = TopikModel::get_all();
    	return view('topik.index', compact('topiks'));
    }

    public function create(){
    	return view('topik.form');
    }

    public function store(Request $request){
    	// return view('item.form');
    	$new_topik = TopikModel::save($request->all());
    	return redirect('/topik');
    }
}
