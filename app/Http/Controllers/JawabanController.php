<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JawabanModel;

class JawabanController extends Controller
{
    public function index(){
    	$jawabans = JawabanModel::get_all();
    	return view('jawaban.index');
    }

    public function create(){
    	return view('jawaban.form');
    }

    public function store(Request $request){
    	// return view('item.form');
    	$new_jawaban = JawabanModel::save($request->all());
    	return redirect('/jawaban');
    }
}
